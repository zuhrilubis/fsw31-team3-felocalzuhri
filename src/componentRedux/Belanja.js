/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/function-component-definition */
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { setItem, setJumlah } from '../redux/keranjangSlice';

export default function Belanja() {
  const dispatch = useDispatch();
  const keranjangRedux = useSelector((state) => state.keranjang);

  return (
    <div style={{ padding: 50 }}>
      <div style={{ margin: 20 }}>Halaman Belanja</div>
      <div style={{
        margin: 20, display: 'flex', flexDirection: 'column', width: 500,
      }}
      >
        <label>nama item:</label>
        <input
          onChange={(e) => {
            // setKeranjang(e.target.value);
            dispatch(setItem(e.target.value));
          }}
        />
        <label>jumlah item:</label>
        <input
          type="number"
          onChange={(e) => {
            // set jumlah didalam redux
            dispatch(setJumlah(e.target.value));
          }}
        />
      </div>
      <div style={{ margin: 20 }}>
        <Link to="/checkout">Checkout!</Link>
      </div>
      <div style={{ margin: 20 }}>
        Item :
        {' '}
        {keranjangRedux.item}
      </div>
      <div style={{ margin: 20 }}>
        Jumlah :
        {' '}
        {keranjangRedux.jumlah}
      </div>
    </div>
  );
}
