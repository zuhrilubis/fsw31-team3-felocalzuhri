/* eslint-disable react/button-has-type */
/* eslint-disable no-console */
/* eslint-disable react/function-component-definition */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { resetItem, resetJumlah } from '../redux/keranjangSlice';

export default function Checkout() {
  const dispatch = useDispatch();

  const keranjangRedux = useSelector((state) => state.keranjang);
  console.log(keranjangRedux);

  return (
    <div style={{ padding: 50 }}>
      <div style={{ margin: 20 }}>Halaman Checkout</div>
      <div style={{ margin: 20 }}>
        Daftar belanja :
        {' '}
        {keranjangRedux.item}
        {' '}
        -
        {' '}
        {keranjangRedux.jumlah}
      </div>
      <div style={{ margin: 20 }}>
        <Link to="/belanja">Kembali Belanja!</Link>
      </div>
      <div>
        <button onClick={() => {
          dispatch(resetItem());
          dispatch(resetJumlah());
        }}
        >
          Kosongkan Kerangjang!
        </button>

      </div>
    </div>
  );
}
