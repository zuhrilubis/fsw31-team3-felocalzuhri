// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
// import { getAnalytics } from 'firebase/analytics';
import { getStorage } from 'firebase/storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyCJxRY0B2dA-Kk0q1yYd7K9_K0RgNFnOMc',
  authDomain: 'team-3-fsw31.firebaseapp.com',
  projectId: 'team-3-fsw31',
  storageBucket: 'team-3-fsw31.appspot.com',
  messagingSenderId: '670860594605',
  appId: '1:670860594605:web:35e613fd6000c50b7b6524',
  measurementId: 'G-QFFT8K61Q5',
};

// Initialize Firebase
const base = initializeApp(firebaseConfig);
// const analytics = getAnalytics(base);

const storage = getStorage(base);

export default storage;
