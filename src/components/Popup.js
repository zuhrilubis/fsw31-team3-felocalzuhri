import React from 'react';

const Popup = ({ message, color }) => <div className="popup" style={{ color, whiteSpace: 'pre-line' }}>{message}</div>;

export default Popup;
