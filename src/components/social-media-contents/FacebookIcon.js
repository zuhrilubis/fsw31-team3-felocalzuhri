import React from 'react';
import picture from '../../assets/facebook.svg';

const FacebookIcon = () => <img className="me-4" src={picture} alt="facebook.com" />;

export default FacebookIcon;
