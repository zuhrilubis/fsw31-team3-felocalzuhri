import React from 'react';
import picture from '../../assets/twitch.svg';

const TwitchIcon = () => (
  <img className="ms-4" src={picture} alt="twitch.com" />
);

export default TwitchIcon;
