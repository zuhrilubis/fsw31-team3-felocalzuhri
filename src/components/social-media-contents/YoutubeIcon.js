import React from 'react';
import picture from '../../assets/vector.svg';

const YoutubeIcon = () => <img className="mx-4" src={picture} alt="youtube.com" />;

export default YoutubeIcon;
