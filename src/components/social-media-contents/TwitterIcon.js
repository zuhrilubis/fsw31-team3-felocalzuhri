import React from 'react';
import picture from '../../assets/twitter.svg';

const TwitterIcon = () => <img className="mx-4" src={picture} alt="twitter.com" />;

export default TwitterIcon;
