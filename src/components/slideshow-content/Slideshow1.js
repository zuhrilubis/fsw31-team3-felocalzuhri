import React from 'react';
import picture from '../../assets/rockpaperstrategy-1600.jpg';

const Slideshow1 = () => (
  <div className="carousel-item active">
    <img
      src={picture}
      className="d-block w-100"
      alt="rock, paper, scissors"
    />
  </div>
);

export default Slideshow1;
