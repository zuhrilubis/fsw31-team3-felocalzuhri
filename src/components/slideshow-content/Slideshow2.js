import React from 'react';
import picture from '../../assets/blankimage.jpg';

const Slideshow2 = () => (
  <div className="carousel-item">
    <img src={picture} className="d-block w-100" alt="pic" />
  </div>
);

export default Slideshow2;
