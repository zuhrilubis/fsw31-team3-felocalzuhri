import React from 'react';
import '../App.css';

const ButtonLogin = ({
  title, handleClick, disabled, margin,
}) => {
  const buttonTitle = title || 'Button';

  return (
    <button
      className="button"
      type="button"
      style={{ margin }}
      onClick={handleClick}
      disabled={disabled}
    >
      {buttonTitle}
    </button>
  );
};

export default ButtonLogin;
