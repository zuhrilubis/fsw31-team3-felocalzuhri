import React from 'react';

const PlusIcon = () => (
  <i className="bi bi-plus-circle-fill" />
);

export default PlusIcon;
