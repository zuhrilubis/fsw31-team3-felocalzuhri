import React from 'react';

const HistoryIcon = () => <i className="bi bi-clock-history" />;

export default HistoryIcon;
