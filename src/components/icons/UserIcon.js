import React from 'react';

const UserIcon = () => <i className="bi bi-person" />;

export default UserIcon;
