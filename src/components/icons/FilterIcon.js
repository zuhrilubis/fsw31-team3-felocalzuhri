import React from 'react';

const FilterIcon = () => <i className="bi bi-filter-square-fill" style={{ fontSize: '1.5rem', color: '#ffb548' }} />;

export default FilterIcon;
