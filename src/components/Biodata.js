import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import axios from 'axios';
import picture from '../assets/default-profile-picture.webp';
import UpdateBioPage from './UpdateBio';

const Biodata = () => {
  const [image, setImage] = useState(picture);
  const [modalOpen, setModalOpen] = useState(false);

  const closeModal = () => {
    setModalOpen(false);
  };

  // --------------------------added by Zuhri------------------------------
  const [dataBio, setdataBio] = useState('');
  // const [imageSrc, setImageSrc] = useState();
  // const [assetPath, setAssetPath] = useState();
  const tampilanDataBio = async () => {
    const accessTokenYangTersimpan = localStorage.getItem('accessToken');
    const accessToken = `Bearer ${accessTokenYangTersimpan}`;
    try {
      const getData = await axios
        .get('https://team3-be-localzuhri.vercel.app/users/detailUser', {
          headers: { Authorization: `${accessToken}` },
        })
        .then((response) => response.data);
      setdataBio(getData);
      return true;
    } catch (error) {
      return error;
    }
  };

  useEffect(() => {
    tampilanDataBio();
    if (
      dataBio
      && dataBio.User_Biodatum
      && (dataBio.User_Biodatum.photo_url === undefined
        || dataBio.User_Biodatum.photo_url === '')
    ) {
      setImage(picture);
    } else if (
      dataBio
      && dataBio.User_Biodatum
      && dataBio.User_Biodatum.photo_url
    ) {
      setImage(dataBio.User_Biodatum.photo_url);
    }
    setdataBio(dataBio);
  }, [dataBio?.User_Biodatum?.photo_url]);

  return (
    <div className="container-bio" style={{ fontSize: 'large' }}>
      <div className="edit-profile" onClick={() => setModalOpen(true)} aria-hidden="true" role="button" tabIndex={0}>
        <div className="edit-icon">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
            <defs>
              <linearGradient
                gradientUnits="userSpaceOnUse"
                x1="346.57"
                y1="513.8"
                x2="361.57"
                y2="528.8"
              >
                <stop stopColor="#2e5d89" />
                <stop offset="1" stopColor="#1b92f4" />
              </linearGradient>
              <linearGradient
                gradientUnits="userSpaceOnUse"
                y2="517.8"
                x2="0"
                y1="545.8"
              >
                <stop stopColor="#3889e9" />
                <stop offset="1" stopColor="#5ea5fb" />
              </linearGradient>
              <linearGradient
                x1="-324.94"
                y1="649.74"
                x2="-328.21"
                y2="307.75"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor="#016ed4" />
                <stop offset="1" stopColor="#00b1ff" />
              </linearGradient>
            </defs>
            <path
              d="m505.99-87.12v3.12h3.12l7.88-7.88-3.12-3.12zm13.76-7.51c.33-.33.33-.85 0-1.18l-1.95-1.95c-.33-.33-.85-.33-1.18 0l-1.63 1.64 3.12 3.12z"
              fill="#ffb548"
              transform="matrix(1.14224 0 0 1.14224-574.96 114.94)"
              fillOpacity=".839"
              enableBackground="new"
            />
          </svg>
        </div>
      </div>
      <div className="img-profile">
        <img
          src={image}
          alt="default-profile-picuture"
          width="120px"
          height="120px"
          style={{ borderRadius: '50%', borderStyle: 'solid' }}
        />
      </div>

      <table className="marginTop30">
        <tbody>
          <tr>
            <th style={{ width: '10rem' }} alt="top1" />
            <th style={{ width: '0.5rem' }} alt="top2" />
            <th style={{ width: '15rem' }} alt="top3" />
          </tr>
          <tr>
            <td>Username</td>
            <td>:</td>
            <td>{dataBio?.username}</td>
          </tr>
          <tr>
            <td>Email</td>
            <td>:</td>
            <td>{dataBio?.email}</td>
          </tr>
          <tr>
            <td>Fullname</td>
            <td>:</td>
            <td>
              {dataBio?.User_Biodatum?.fullname}
              {' '}
            </td>
          </tr>
          <tr>
            <td>Phone</td>
            <td>:</td>
            <td>{dataBio?.User_Biodatum?.phone_number}</td>
          </tr>
          <tr>
            <td>Address</td>
            <td>:</td>
            <td>{dataBio?.User_Biodatum?.address}</td>
          </tr>
        </tbody>
      </table>

      <Modal
        isOpen={modalOpen}
        className="modalProfile"
        onRequestClose={closeModal}
      >
        <p className="yellow-color">You can update your bio here:</p>
        <div>
          <UpdateBioPage
            closeModal={closeModal}
            userName={dataBio?.username}
            email={dataBio?.email}
            fullName={dataBio?.User_Biodatum?.fullname}
            phoneNumber={dataBio?.User_Biodatum?.phone_number}
            address={dataBio?.User_Biodatum?.address}
            photoURL={dataBio?.User_Biodatum?.photo_url}
          />
        </div>
      </Modal>
    </div>
  );
};

export default Biodata;
