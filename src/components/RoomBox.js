import React from 'react';
import '../App.css';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

const RoomBox = (props) => {
  const navigate = useNavigate();
  const userRedux = useSelector((state) => state.user);

  const { roomId } = props; // room id
  const { title } = props; // room name
  const { player1 } = props; // player 1 username
  const { status } = props;
  const { pemenang } = props;

  // let player1_choice = props.player1_choice;
  // let player2 = props.player2;
  // let player2_choice = props.player2_choice;

  const handleClick = () => {
    if (status === 'Complete') {
      navigate(`/complete-room/${roomId}`);
    } else if (status === 'available') {
      if (player1 === userRedux.username) {
        navigate(`/my-room/${roomId}`);
      } else {
        navigate(`/available-room/${roomId}`);
      }
    }
  };

  return (
    <div className="room-box" onClick={handleClick} aria-hidden="true" role="button" tabIndex={0}>
      <div className="title-room">{title}</div>
      <div className="content-room">
        <div>
          <p>
            Player 1 :
            {' '}
            {player1}
          </p>
          <p>
            Pemenang:
            {' '}
            {pemenang}
          </p>
          <p>
            Status:
            {' '}
            {status}
          </p>

        </div>
        <div />
      </div>
    </div>
  );
};

export default RoomBox;
