/* eslint-disable no-use-before-define */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState, useEffect } from 'react';
// import { useNavigate } from "react-router-dom";
import { useFormik } from 'formik';
import axios from 'axios';
import { ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import { useSelector } from 'react-redux';
import storage from '../config/firebase.config';
import picture from '../assets/default-profile-picture.webp';

const UpdateBioPage = ({
  closeModal,
  userName,
  email,
  fullName,
  phoneNumber,
  address,
  photoURL,
}) => {
  // const navigate = useNavigate();
  const [user, setUser] = useState({
    username: userName,
    email,
    fullname: fullName,
    phonenumber: phoneNumber,
    address,
    photo_url: photoURL || picture,
  });
  const [image, setImage] = useState(null);
  const userRedux = useSelector((state) => state.user);

  const accessTokenYangTersimpan = localStorage.getItem('accessToken');
  const accessToken = `Bearer ${accessTokenYangTersimpan}`;
  // const [modalOpen, setModalOpen] = useState(true);

  const handleUpdateBio = async () => {
    let imgRef = null;
    let assetPath = '';
    const userId = userRedux.id;
    const now = Math.floor(Date.now() / 1000);
    const formattedTime = now.toString();

    if (image) {
      imgRef = ref(storage, `assets/${userId}/${formattedTime}${image.name}`);
      await uploadBytes(imgRef, image);
      const downloadURL = await getDownloadURL(imgRef);
      assetPath = downloadURL;
    }

    const updatedUser = {
      ...user,
      fullname: formik.values.fullname,
      phonenumber: formik.values.phonenumber,
      address: formik.values.address,
      photo_url: assetPath || user.photo_url,
    };

    setUser(updatedUser);

    try {
      const dataToUpdate = {
        fullname: formik.values.fullname,
        phone_number: formik.values.phonenumber,
        address: formik.values.address,
        photo_url: assetPath || user.photo_url,
      };

      await axios
        .put(
          'https://team3-be-localzuhri.vercel.app/users/updateBio',
          dataToUpdate,
          {
            headers: { Authorization: `${accessToken}` },
          },
        )
        .then((response) => response.data.message);

      alert(
        'Success, your biodata is being updated, refresh the page to see the changes',
      );
      closeModal();
      window.location.reload();
    } catch (error) {
      // console.log(error);
      alert('Oops..Update biodata failed');
    }
  };

  const formik = useFormik({
    initialValues: {
      fullname: user.fullname,
      phonenumber: user.phonenumber,
      address: user.address,
      photo_url: user.photo_url,
    },
    onSubmit: handleUpdateBio,
  });

  useEffect(() => {
    // console.log(user);
    // console.log(image);
  }, [user, image]);

  // console.log(formik);

  return (
    <div>
      <form onSubmit={formik.handleSubmit} className="auth-form">
        <div>
          <table>
            <tbody>
              <tr>
                <td>
                  {user.username}
                  <br />
                  {user.email}
                </td>
                <td>
                  {' '}
                  <img
                    src={image ? URL.createObjectURL(image) : user.photo_url}
                    alt="default-profile-picuture"
                    width="120px"
                    height="120px"
                    style={{
                      borderRadius: '50%',
                      borderStyle: 'solid',
                      marginLeft: '30px',
                      objectFit: 'contain',
                    }}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="input-field nobg">
          <label>Profile picture: </label>
          <input
            className="input_picture"
            type="file"
            placeholder="Photo Profile"
            name="photo_url"
            required={false}
            onChange={(event) => {
              const file = event.target.files[0];
              if (file) {
                setImage(file);
              }
            }}
          />
        </div>
        <label>Fullname: </label>
        <div className="input-field">
          <input
            type="text"
            name="fullname"
            value={formik.values.fullname}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            required
            minLength={7}
          />
        </div>

        <label>Phone number: </label>
        <div className="input-field">
          <input
            type="integer"
            name="phonenumber"
            value={formik.values.phonenumber}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            required
            minLength={10}
          />
        </div>
        <label>Address: </label>
        <div className="input-field">
          <input
            type="text"
            name="address"
            value={formik.values.address}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            required
          />
        </div>
        <div className="buttonUpdateBio">
          <button className="button" type="submit" style={{ marginTop: '2em' }}>
            UPDATE
          </button>
        </div>
      </form>
    </div>
  );
};

export default UpdateBioPage;
