import React from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
// import jwtDecode from "jwt-decode";
// import { setUsername, setUserId } from "../redux/userSlice";
import UserIcon from './icons/UserIcon';

const Nav = () => {
  const location = useLocation();
  const navigate = useNavigate();
  // It shows username of the current user
  const accessToken = localStorage.getItem('accessToken');
  /* const dispatch = useDispatch();
  const userRedux = useSelector((state) => state.user);
  if (accessToken) {
    const decodedToken = jwtDecode(accessToken);
    dispatch(setUsername(decodedToken.username));

  } */

  const username = useSelector((state) => state.user.username);

  let navLogo;
  let navTitle;
  let navContent;
  let currentUser;

  const handleLogout = () => {
    // Remove the accessToken from localStorage
    localStorage.removeItem('accessToken');
    localStorage.removeItem('username');
    // isLoggedIn = false;

    // Redirect to the home page or any other desired location
    navigate('/');
  };

  if (location.pathname === '/' || location.pathname === '/login') {
    navLogo = (
      <Link to="/" className="logo navbar-brand mx-5 px-4">
        logo
      </Link>
    );
    navTitle = (
      <Link to="/dashboard" className="nav-link" aria-current="page">
        dashboard
      </Link>
    );
    if (accessToken) {
      if (accessToken === null || accessToken === 'null') {
        navContent = (
          <Link to="/register" className="nav-link active">
            sign up
          </Link>
        );
      } else {
        navContent = (
          <Link to="/" className="nav-link active" onClick={handleLogout}>
            Log out
          </Link>
        );
        currentUser = (
          <li className="nav-item mx-2" style={{ padding: '8px' }}>
            <UserIcon />
            {' '}
            {username}
          </li>
        );
      }
    } else {
      navContent = (
        <Link to="/register" className="nav-link active">
          sign up
        </Link>
      );
    }
  } else if (location.pathname === '/register') {
    navLogo = (
      <Link to="/" className="logo navbar-brand mx-5 px-4">
        logo
      </Link>
    );
    navTitle = (
      <Link to="/dashboard" className="nav-link" aria-current="page">
        dashboard
      </Link>
    );

    /* navContent = (
      <Link to="/login" className="nav-link active">
        sign in
      </Link>
    ); */
    if (accessToken) {
      if (accessToken === null || accessToken === 'null') {
        navContent = (
          <Link to="/register" className="nav-link active">
            sign up
          </Link>
        );
      } else {
        navContent = (
          <Link to="/" className="nav-link active" onClick={handleLogout}>
            Log out
          </Link>
        );
        currentUser = (
          <li className="nav-item mx-2" style={{ padding: '8px' }}>
            <UserIcon />
            {' '}
            {username}
          </li>
        );
      }
    } else {
      navContent = (
        <Link to="/login" className="nav-link active">
          sign in
        </Link>
      );
    }
  } else {
    navLogo = (
      <Link to="/dashboard" className="logo navbar-brand mx-3 px-4">
        dashboard
      </Link>
    );
    navTitle = (
      <Link to="/" className="nav-link" aria-current="page">
        home
      </Link>
    );
    navContent = (
      <Link to="/" className="nav-link active" onClick={handleLogout}>
        Log out
      </Link>
    );
    currentUser = (
      <li className="nav-item mx-2" style={{ padding: '8px' }}>

        <UserIcon />
        {' '}
        {username}
      </li>
    );
  }

  return (
    <div>
      <nav className="navbar navbar-dark navbar-expand-lg fw-bold text-uppercase">
        <div className="container-fluid">
          {navLogo}
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-5 ps-4">
              <li className="nav-item mx-4">{navTitle}</li>
              <li className="nav-item mx-4">
                <Link to="/" className="nav-link">
                  work
                </Link>
              </li>
              <li className="nav-item mx-4">
                <Link to="/" className="nav-link text-uppercase">
                  contact
                </Link>
              </li>
              <li className="nav-item mx-4">
                <Link to="/" className="nav-link">
                  about me
                </Link>
              </li>
            </ul>
            <ul className="navbar-nav me-5 pe-5">
              {currentUser}
              <li className="nav-item mx-2">{navContent}</li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Nav;
