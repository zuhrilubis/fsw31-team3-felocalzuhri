import React, { useEffect, useState } from 'react';
import axios from 'axios';
import RoomBox from './RoomBox';
import FilterIcon from './icons/FilterIcon';

const RoomList = () => {
  const [roomData, setRoomData] = useState([]);
  const [showAvailableRooms, setShowAvailableRooms] = useState(false);
  const [showCompleteRooms, setShowCompleteRooms] = useState(false);
  const [showUserRooms, setShowUserRooms] = useState(false);
  const [showUserGames, setShowUserGames] = useState(false);
  const [showAllRooms, setShowAllRooms] = useState(true);
  const [showFilterItems, setShowFilterItems] = useState(false);
  const [showFilterButton, setShowFilterButton] = useState(false);
  const username = localStorage.getItem('username');

  const handleShowFilter = () => {
    setShowFilterButton(!showFilterButton);
  };

  const handleShowAvailableRooms = () => {
    setShowAvailableRooms(true);
    setShowCompleteRooms(false);
    setShowUserRooms(false);
    setShowUserGames(false);
    setShowAllRooms(false);
  };

  const handleShowCompleteRooms = () => {
    setShowAvailableRooms(false);
    setShowCompleteRooms(true);
    setShowUserRooms(false);
    setShowUserGames(false);
    setShowAllRooms(false);
  };

  const handleMyRooms = () => {
    setShowAvailableRooms(false);
    setShowCompleteRooms(false);
    setShowUserRooms(true);
    setShowUserGames(false);
    setShowAllRooms(false);
  };

  const handleMyGames = () => {
    setShowAvailableRooms(false);
    setShowCompleteRooms(false);
    setShowUserRooms(false);
    setShowUserGames(true);
    setShowAllRooms(false);
  };

  const handleShowAllRooms = () => {
    setShowAvailableRooms(false);
    setShowCompleteRooms(false);
    setShowUserRooms(false);
    setShowUserGames(false);
    setShowAllRooms(true);
  };

  useEffect(() => {
    const tampilanRooms = async () => {
      const accessTokenYangTersimpan = localStorage.getItem('accessToken');
      const accessToken = `Bearer ${accessTokenYangTersimpan}`;
      try {
        const response = await axios.get(
          'https://team3-be-localzuhri.vercel.app/game/rooms',
          {
            headers: { Authorization: `${accessToken}` },
          },
        );
        setRoomData(response.data);
        return true; // Return true to indicate success
      } catch (error) {
        return error; // Return the error object in case of error
      }
    };

    tampilanRooms();
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setShowFilterItems(true);
    }, 2000);

    return () => {
      clearTimeout(timeout);
    };
  }, []);

  let filteredRooms = roomData;

  if (showUserRooms) {
    filteredRooms = filteredRooms.filter(
      (room) => room.player1.username === username,
    );
  } else if (showUserGames) {
    filteredRooms = filteredRooms.filter(
      (room) => room.player1.username === username
        || room.player2?.username === username,
    );
  } else if (showAvailableRooms) {
    filteredRooms = filteredRooms.filter(
      (room) => room.status === 'available',
    );
  } else if (showCompleteRooms) {
    filteredRooms = filteredRooms.filter(
      (room) => room.status === 'Complete',
    );
  }

  return (
    <div className="rooms" style={{ textDecoration: 'none' }}>
      <div className="wrapFilter">
        {showFilterItems && (<div className="showFilter" onClick={handleShowFilter} aria-hidden="true" role="button" tabIndex={0}><FilterIcon /></div>)}
        <div className="filterbutton">
          {showFilterButton && (
          <>
            <div className="filterItem" onClick={handleShowAllRooms} aria-hidden="true" role="button" tabIndex={-1}>All</div>
            <div className="filterItem" onClick={handleShowAvailableRooms} aria-hidden="true" role="button" tabIndex={-2}>Available Rooms</div>
            <div className="filterItem" onClick={handleShowCompleteRooms} aria-hidden="true" role="button" tabIndex={-3}>Complete Rooms</div>
            <div className="filterItem" onClick={handleMyRooms} aria-hidden="true" role="button" tabIndex={-4}>My Rooms</div>
            <div className="filterItem" onClick={handleMyGames} aria-hidden="true" role="button" tabIndex={-5}>My Games</div>
          </>
          )}
        </div>
      </div>

      {filteredRooms.map((room) => (
        <RoomBox
          key={room.id}
          roomId={room.id}
          title={room.room_name}
          player1={room.player1.username}
          player2={room.player2?.username}
          status={room.status}
          pemenang={room.pemenang === null ? '.........' : room.pemenang}
        />
      ))}
    </div>
  );
};

export default RoomList;
