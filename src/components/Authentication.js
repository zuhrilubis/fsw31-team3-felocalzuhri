import React from 'react';
import { Navigate } from 'react-router-dom';

const Authentication = ({ children }) => {
  const accessToken = localStorage.getItem('accessToken');

  if (accessToken) {
    if (accessToken === null || accessToken === 'null') {
      return <Navigate to="/login" />;
    }
    return <div>{children}</div>;
  }

  return <Navigate to="/login" />;
};

export default Authentication;
