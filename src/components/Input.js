import React from 'react';
import '../App.css';

const Input = (props) => {
  const inputType = props.type ?? 'Text';
  const inputPlaceholder = props.placeholder ?? 'enter text here';

  return (
    <div className="input-field">
      <input
        type={inputType}
        placeholder={inputPlaceholder}
        onChange={props.handleClick}
      />
    </div>
  );
};

export default Input;
