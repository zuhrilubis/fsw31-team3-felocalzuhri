/* eslint-disable no-unused-expressions */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-undef */
const { render, screen } = require('@testing-library/react');
const { default: Popup } = require('../Popup');

/* eslint-disable eol-last */
test('should render the popup with the provided message and color', () => {
  const message = 'This is a test message.';
  const color = 'red';
  const { getByText } = render(
    <Popup message={message} color={color} />,
  );
  const popupElement = getByText(message);
  expect(popupElement).toBeInTheDocument();
  expect(popupElement).toHaveStyle(`color: ${color}`);
});