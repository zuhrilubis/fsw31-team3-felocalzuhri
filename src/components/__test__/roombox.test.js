/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-undef */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';
import RoomBox from '../RoomBox';

// Mocking react-redux useSelector
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

describe('RoomBox Component', () => {
  beforeEach(() => {
    useSelector.mockReturnValue({ username: 'testuser' }); // Mock the Redux selector
  });

  it('should render the room information correctly', () => {
    const props = {
      roomId: '123',
      title: 'Test Room',
      player1: 'user1',
      status: 'available',
      pemenang: 'winner',
    };

    const { getByText } = render(
      <MemoryRouter>
        <RoomBox {...props} />
      </MemoryRouter>,
    );

    expect(getByText(props.title)).toBeInTheDocument();
    expect(getByText(`Player 1 : ${props.player1}`)).toBeInTheDocument();
    expect(getByText(`Pemenang: ${props.pemenang}`)).toBeInTheDocument();
    expect(getByText(`Status: ${props.status}`)).toBeInTheDocument();
  });
});
