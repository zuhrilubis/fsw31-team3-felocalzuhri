/* eslint-disable no-unused-expressions */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-undef */
const { render, screen } = require('@testing-library/react');
const { default: Input } = require('../Input');

/* eslint-disable eol-last */
test('should render input default input', async () => {
  render(<Input />);
  const textPlaceholder = screen.getByPlaceholderText('enter text here');
  expect(textPlaceholder).toBeInTheDocument();
});

test('should render input with placeholder password', async () => {
  render(<Input type="password" placeholder="password" />);
  const textPlaceholder = screen.getByPlaceholderText('password');
  const inputType = textPlaceholder.getAttribute('type');
  expect(textPlaceholder).toBeInTheDocument();
  expect(inputType).toBe('password');
});