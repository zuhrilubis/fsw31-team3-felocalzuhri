import React from 'react';
import picture from '../assets/skull.png';

const Skull = () => (
  <div className="d-flex align-items-end skull">
    <img src={picture} alt="skull" id="skull" />
  </div>
);

export default Skull;
