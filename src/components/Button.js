import React from 'react';
import '../App.css';

const Button = (props) => {
  const buttonTitle = props.title ?? 'Button';
  const buttonIcon = props.icon ?? null;

  return (
    <button
      className="button"
      type="button"
      onClick={props.handleClick}
    >
      {buttonIcon}
      {buttonTitle}
    </button>
  );
};

export default Button;
