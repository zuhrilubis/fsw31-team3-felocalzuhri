import React from 'react';
import picture from '../../assets/jada-griffin.jpg';

const ProfilePictCard2 = () => <img src={picture} alt="Avatar" className="avatar" />;

export default ProfilePictCard2;
