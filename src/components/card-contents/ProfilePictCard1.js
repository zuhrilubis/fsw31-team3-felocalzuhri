import React from 'react';
import picture from '../../assets/evan-lahti.jpg';

const ProfilePictCard1 = () => <img src={picture} alt="Avatar" className="avatar" />;

export default ProfilePictCard1;
