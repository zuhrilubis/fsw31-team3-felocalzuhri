import React from 'react';
import picture from '../../assets/aaron-williams.jpg';

const ProfilePictCard3 = () => <img src={picture} alt="Avatar" className="avatar" />;

export default ProfilePictCard3;
