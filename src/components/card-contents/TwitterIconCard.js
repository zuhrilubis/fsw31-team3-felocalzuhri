import React from 'react';
import picture from '../../assets/twitter-card.png';

const TwitterIconCard = () => <img src={picture} alt="twitter.com" className="ms-auto avatar-twt" />;

export default TwitterIconCard;
