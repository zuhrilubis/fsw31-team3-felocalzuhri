import { configureStore } from '@reduxjs/toolkit';
// eslint-disable-next-line import/no-named-as-default
import userSlice from './userSlice';

const store = configureStore({
  reducer: {
    user: userSlice,
  },
});

export default store;
