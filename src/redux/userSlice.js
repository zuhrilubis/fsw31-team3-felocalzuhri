/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import jwtDecode from 'jwt-decode';

const persistedUsername = localStorage.getItem('username');

const initialState = {
  username: persistedUsername || '',
  loading: false,
  error: null,
};

export const loginFecth = createAsyncThunk('user/login', async ({ username, password }) => {
  try {
    const backendResponse = await axios.post('https://team3-be.vercel.app/users/login', {
      username,
      password,
    });

    const token = backendResponse.data.accessToken;
    localStorage.setItem('accessToken', token);
    const decodedToken = jwtDecode(token);
    return decodedToken.username; // Return the decoded username directly
  } catch (error) {
    throw error.response.data.message;
  }
});

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUsername: (state, action) => {
      state.username = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginFecth.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(loginFecth.fulfilled, (state, action) => {
        // Set the state.username to the fulfilled value (decoded username)
        state.username = action.payload;

        state.loading = false;
        localStorage.setItem('username', action.payload);
      })
      .addCase(loginFecth.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'An unknown error occurred.';
      });
  },
});

export const { setUsername } = userSlice.actions;

export default userSlice.reducer;
