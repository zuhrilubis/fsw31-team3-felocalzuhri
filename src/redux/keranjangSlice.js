/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  item: '',
  //   jumlah item
  jumlah: '',
};

export const keranjangSlice = createSlice({
  name: 'keranjang',
  initialState,
  reducers: {
    setItem: (state, action) => {
      state.item = action.payload;
    },
    resetItem: (state) => {
      state.item = '';
    },
    // action untuk update jumlah item
    setJumlah: (state, action) => {
      state.jumlah = action.payload;
    },
    resetJumlah: (state) => {
      state.jumlah = '';
    },
  },
});

export const {
  setItem, resetItem, setJumlah, resetJumlah,
} = keranjangSlice.actions;

export default keranjangSlice.reducer;
