/* eslint-disable global-require */
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import Nav from '../components/Nav';

const CompleteRoom = () => {
  const { roomId } = useParams();
  const [room, setRoom] = useState(null);
  const [result, setResult] = useState('');

  const getRoom = async () => {
    const accessTokenYangTersimpan = localStorage.getItem('accessToken');
    const accessToken = `Bearer ${accessTokenYangTersimpan}`;
    try {
      const response = await axios.get(
        `https://team3-be-localzuhri.vercel.app/game/complete/${roomId}`,
        {
          headers: { Authorization: `${accessToken}` },
        },
      );
      setRoom(response.data);
      // console.log(response.data);
      if (
        response.data.pemenang === 'DRAW'
        || response.data.pemenang === 'draw'
      ) {
        setResult('DRAW');
      } else {
        setResult(`${response.data.pemenang} WIN`);
      }

      return true;
    } catch (error) {
      // console.log(error);
      return error;
    }
  };

  useEffect(() => {
    getRoom();
  }, []); // Run once on component mount

  return (
    <div className="playerVsComContainer">
      <Nav />
      <h1 className="roomName">
        Room:
        {' '}
        {room && room.roomName}
      </h1>
      <div className="player1Vsplayer2-content-matchVsCom">
        <div className="player1Vsplayer2-content-leftVsCom">
          <div className="leftTitlePlayer">{room && room.username1}</div>
          <div className="player1Choices">
            <div>
              <img
                className="complete-room-p1"
                src={require('../assets/batu.png')}
                alt="batu"
                style={{
                  backgroundColor:
                    room && room.pilihanP1 === 'rock' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="complete-room-p1"
                src={require('../assets/kertas.png')}
                alt="kertas"
                style={{
                  backgroundColor:
                    room && room.pilihanP1 === 'paper' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="complete-room-p1"
                src={require('../assets/gunting.png')}
                alt="gunting"
                style={{
                  backgroundColor:
                    room && room.pilihanP1 === 'scissors' ? '#ffb548' : '',
                }}
              />
            </div>
          </div>
        </div>
        <div className="player1Vsplayer2-content-middleVsCom">
          <div
            className="middleVsCom-box"
            style={{
              backgroundColor: 'green',
              border: 'none',
              fontWeight: 'bold',
            }}
          >
            {result}
          </div>
        </div>
        <div className="player1Vsplayer2-content-rightVsCom">
          <div className="rightTitlePlayer">{room && room.username2}</div>
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightVsCom"
                src={require('../assets/batu.png')}
                alt="batu"
                style={{
                  backgroundColor:
                    room && room.pilihanP2 === 'rock' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightVsCom"
                src={require('../assets/kertas.png')}
                alt="kertas"
                style={{
                  backgroundColor:
                    room && room.pilihanP2 === 'paper' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-rightVsCom"
                src={require('../assets/gunting.png')}
                alt="gunting"
                style={{
                  backgroundColor:
                    room && room.pilihanP2 === 'scissors' ? '#ffb548' : '',
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompleteRoom;
