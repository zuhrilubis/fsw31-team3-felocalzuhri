import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Input from '../components/Input';
import Button from '../components/Button';
import rock from '../assets/batu.png';
import paper from '../assets/kertas.png';
import scissors from '../assets/gunting.png';
import Nav from '../components/Nav';
import Popup from '../components/Popup';

const CreateRoom = () => {
  const [myRoomName, setMyRoomName] = useState(null);
  const [choice, setChoice] = useState(null);
  const [isChoiceClicked, setIsChoiceClicked] = useState(false);
  const [message, setMessage] = useState('');
  const [messageColor, setMessageColor] = useState('');
  const [showPopup, setShowPopup] = useState(false);
  const navigate = useNavigate();
  const [roomId, setRoomId] = useState('');

  const clickChoice = (e) => {
    if (!isChoiceClicked) {
      setChoice(e.target.alt);
    }
  };

  useEffect(() => {
    let timeoutId;

    if (showPopup) {
      timeoutId = setTimeout(() => {
        setShowPopup(false);
        if (message === 'Success creating new room!') {
          navigate(`/my-room/${roomId}`);
        }
      }, 2000);
    }

    return () => {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
    };
  }, [showPopup, message, navigate, roomId]);

  const saveRoom = async () => {
    const accessToken = localStorage.getItem('accessToken');
    try {
      const responseAPI = await axios.post(
        'https://team3-be-localzuhri.vercel.app/game/create-room',
        {
          roomName: myRoomName,
          pilihanP1: choice,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        },
      );

      setRoomId(responseAPI.data.room_id);
      setMessage(responseAPI.data.message);
      setMessageColor('#ffb548');
      setIsChoiceClicked(true);
      setShowPopup(true);
    } catch (err) {
      setMessage(err.response.data.message);
      setMessageColor('red');
      setShowPopup(true);
    }
  };

  return (
    <div className="create-new-room">
      <Nav />
      <div className="create-room-content">
        <div className="room-name">
          <Input
            fontSize="1.5rem"
            padding="0.5rem 1.5rem"
            type="text"
            placeholder="input the room name"
            handleClick={(e) => {
              setMyRoomName(e.target.value);
            }}
          />
        </div>
        <div>
          <div className="your-choice">your choice</div>
          <div className="choices">
            <div
              className="box rock"
              id="rock"
              role="button"
              tabIndex={0}
              style={{
                backgroundColor: choice === 'rock' ? '#f3af34' : 'transparent',
                borderRadius: '30px',
              }}
              onClick={clickChoice}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  clickChoice();
                }
              }}
            >
              <img src={rock} alt="rock" />
            </div>
            <div
              className="box paper"
              id="paper"
              role="button"
              tabIndex={0}
              style={{
                backgroundColor: choice === 'paper' ? '#f3af34' : 'transparent',
                borderRadius: '30px',
              }}
              onClick={clickChoice}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  clickChoice();
                }
              }}
            >
              <img src={paper} alt="paper" />
            </div>
            <div
              className="box rock"
              id="scissors"
              role="button"
              tabIndex={0}
              style={{
                backgroundColor:
                  choice === 'scissors' ? '#f3af34' : 'transparent',
                borderRadius: '30px',
              }}
              onClick={clickChoice}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  clickChoice();
                }
              }}
            >
              <img src={scissors} alt="scissors" />
            </div>
          </div>

          <div className="btn-save">
            <Button title="save" handleClick={saveRoom} />
          </div>
        </div>
      </div>
      {showPopup && <Popup message={message} color={messageColor} />}
    </div>
  );
};

export default CreateRoom;
