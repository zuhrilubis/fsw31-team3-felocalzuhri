import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import ButtonLogin from '../components/ButtonLogin';
import Input from '../components/Input';
import Popup from '../components/Popup';
import Nav from '../components/Nav';

const Registration = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');
  const [messageColor, setMessageColor] = useState('');
  const [showPopup, setShowPopup] = useState(false);
  const navigate = useNavigate();
  const [disabled, setDisabled] = useState(true);
  // const [registrationSuccess, setRegistrationSuccess] = useState(false);

  useEffect(() => {
    let timeout;

    if (showPopup) {
      timeout = setTimeout(() => {
        setShowPopup(false);
        if (message === 'registration success') {
          navigate('/login');
        }
      }, 1000);
    }

    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [showPopup, message, navigate]);

  useEffect(() => {
    const areFieldsEmpty = !username || !password || !email;
    setDisabled(areFieldsEmpty);
  }, [username, password, email]);

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const registerFunction = async (event) => {
    event.preventDefault();
    try {
      // Send POST request to the API
      const response = await axios.post('https://team3-be-localzuhri.vercel.app/users/daftar', {
        username,
        email,
        password,
      });

      if (response.data.message === 'registration success') {
        setUsername('');
        setEmail('');
        setPassword('');
      }
      // if (response.data.message === "registration success") {
      //  setRegistrationSuccess(true);
      // }
      setMessageColor('#ffb548');
      setMessage(response.data.message);
      setShowPopup(true);
    } catch (error) {
      setMessage(
        `An error occurred during registration,\n${error.response.data.message}`,
      );
      setMessageColor('red');
      setShowPopup(true);
    }
  };

  /* useEffect(() => {
    if (registrationSuccess) {
      setUsername("");
      setEmail("");
      setPassword("");
      setRegistrationSuccess(false);
    }
  }, [registrationSuccess]); */
  return (
    <div className="authentication-container">
      <Nav />
      <div className="auth-content">
        <div className="form-container">
          <form className="auth-form">
            <h2 className="title">Sign Up</h2>
            <Input
              type="text"
              placeholder="username"
              handleClick={handleUsernameChange}
            />
            <Input
              type="email"
              placeholder="email"
              handleClick={handleEmailChange}
            />
            <Input
              type="password"
              placeholder="password"
              handleClick={handlePasswordChange}
            />
            <p style={{ fontSize: '12px', fontStyle: 'italic' }}>Password should be at least 8 characters</p>

            <ButtonLogin
              margin="30px 0 30px 0"
              title="Register"
              handleClick={registerFunction}
              disabled={disabled}
            />

          </form>
          <div className="bottom-link">
            <Link to="/login">
              <p>already have an account?</p>
            </Link>
          </div>
        </div>
      </div>
      {showPopup && <Popup message={message} color={messageColor} />}
    </div>
  );
};

export default Registration;
