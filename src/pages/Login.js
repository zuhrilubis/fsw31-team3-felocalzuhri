import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
// import axios from 'axios';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ButtonLogin from '../components/ButtonLogin';
import Input from '../components/Input';
import Popup from '../components/Popup';
import Nav from '../components/Nav';
import { loginFecth } from '../redux/userSlice';

const Login = () => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const navigate = useNavigate();
  const [showPopup, setShowPopup] = useState(false);
  const [message, setMessage] = useState('');
  const [messageColor, setMessageColor] = useState('');
  const [disabled, setDisabled] = useState(true);
  const dispatch = useDispatch();

  // Access the error state from Redux
  // const userError = useSelector((state) => state.user.error);
  useEffect(() => {
    let timeout;

    if (showPopup) {
      timeout = setTimeout(() => {
        setShowPopup(false);
      }, 2000);
    }

    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [showPopup, message]);

  // Update the input handlers to enable/disable the submit button
  useEffect(() => {
    setDisabled(!username || !password);
  }, [username, password]);

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleLogin = async (e) => {
    e.preventDefault();
    // Check if the password is not more than 8 characters
    if (password.length < 8) {
      setMessage('Password should be at least 8 characters');
      setMessageColor('red');
      setShowPopup(true);
      return;
    }
    try {
      const resultAction = await dispatch(loginFecth({ username, password }));
      unwrapResult(resultAction);
      // If the login is successful, you can navigate to the dashboard or do any other action.
      navigate('/dashboard');
    } catch (error) {
      // Display the error message from Redux
      setMessage('Wrong username/password');
      setMessageColor('red');
      setShowPopup(true);
    }
  };

  return (
    <div className="authentication-container">
      <Nav />
      <div className="auth-content">
        <div className="form-container">
          <form className="auth-form">
            <h2 className="title" style={{ marginBottom: '40px' }}>
              Sign In
            </h2>
            <Input
              type="text"
              placeholder="username"
              handleClick={handleUsernameChange}
            />
            <Input
              type="password"
              placeholder="password"
              handleClick={handlePasswordChange}
            />
            <p style={{ fontSize: '12px', fontStyle: 'italic' }}>
              Password should be at least 8 characters
            </p>

            <ButtonLogin
              margin="30px 0 30px 0"
              title="Sign in"
              handleClick={handleLogin}
              disabled={disabled}
            />
          </form>
          <div className="bottom-link">
            <Link to="/register">
              <p>new here?</p>
            </Link>
          </div>
        </div>
      </div>
      {showPopup && <Popup message={message} color={messageColor} />}
    </div>
  );
};

export default Login;
