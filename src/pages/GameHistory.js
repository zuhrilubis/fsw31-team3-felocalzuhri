import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import Nav from '../components/Nav';
import Button from '../components/Button';

const GameHistyoryPage = () => {
  const [dataHistory, setDataHistory] = useState([]);
  const accessToken = localStorage.getItem('accessToken');
  const decodedToken = jwtDecode(accessToken);
  const idUser = decodedToken.id;
  async function tampilanHistory() {
    try {
      const getData = await axios
        .get('https://team3-be-localzuhri.vercel.app/game/history', {
          headers: { Authorization: `Bearer ${accessToken}` },
        })
        .then((response) => response.data);

      setDataHistory(getData);
      return true;
    } catch (error) {
      // console.log(error);
      return error;
    }
  }
  const formatDate = (dateString) => {
    const formattedDateTime = new Date(dateString).toLocaleString('en-US', {
      timeZone: 'Asia/Jakarta',
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    });
    return formattedDateTime;
  };

  useEffect(() => {
    tampilanHistory();
  }, []);

  useEffect(() => {
    // console.log(dataHistory);
  }, [dataHistory]);

  const sortedDataHistory = [...dataHistory].sort(
    (a, b) => new Date(b.updatedAt) - new Date(a.updatedAt),
  );
  // eslint-disable-next-line no-unreachable
  const printPDF = async () => {
    try {
      const response = await axios.get(`https://team3-be-localzuhri.vercel.app/game/pdf/${idUser}`, {
        responseType: 'blob', // Set the response type to 'blob'
      });

      // Create a Blob from the response data
      const blob = new Blob([response.data], { type: 'application/pdf' });

      // Create a URL for the Blob
      const url = URL.createObjectURL(blob);

      // Create a temporary link element to trigger the download
      const link = document.createElement('a');
      link.href = url;
      link.target = '_blank'; // Open the link in a new tab
      link.download = 'output.pdf'; // Set the download filename
      link.click(); // Simulate a click on the link

      // Clean up the URL and link
      URL.revokeObjectURL(url);
      link.remove();

      return true;
    } catch (error) {
      // console.log(error);
      return error;
    }
  };

  return (
    <div className="gameHistory-Container">
      <Nav />
      <div className="gameHistory-container-below">
        <div className="gameHistory-content">
          <div className="gameHistory-title">GAME HISTORY</div>
          <div className="wrap-history">
            <div className="col-1h">Name Room</div>
            <div className="col-2h">Time</div>
            <div className="col-3h">Status</div>
          </div>
          <div className="tabel-gameHistory">
            <div className="container-gameHistoryUser">
              {sortedDataHistory && sortedDataHistory.length === 0 ? (
                <p>
                  You do not have any game history. Go to
                  {' '}
                  <Link to="/dashboard" style={{ color: '#ffb548' }}>
                    dashboard
                  </Link>
                  {' '}
                  to play a game.
                </p>
              ) : (
                sortedDataHistory.map((x) => (
                  <div className="singleHistoryTable">
                    <div className="col-1">{x.room_name}</div>
                    <div className="col-2">{formatDate(x.updatedAt)}</div>
                    <div className="col-3">
                      {x.player1_id === decodedToken.id ? x.hasil_1 : x.hasil_2}
                    </div>
                  </div>
                ))
              )}
            </div>
            <div className="pdf-div"><Button title="Print as PDF" handleClick={printPDF} /></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GameHistyoryPage;
