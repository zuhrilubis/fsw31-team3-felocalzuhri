/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */

import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import rock from '../assets/batu.png';
import paper from '../assets/kertas.png';
import scissors from '../assets/gunting.png';

import Nav from '../components/Nav';

const AvailableRoom = () => {
  const { roomId } = useParams();
  const [room, setRoom] = useState(null);
  const navigate = useNavigate();

  // const { roomId } = useParams();
  // const roomDetail = roomDetail[roomId - 1];
  // const accessToken = localStorage.getItem("accessToken");
  const [pilihanP1, setPilihanP1] = useState('');
  const [pilihanP2, setPilihanP2] = useState('');
  const [middleColor, setmiddleColor] = useState('');
  const [border, setborder] = useState('');
  const [result, setResult] = useState('');
  const [fontWeight, setfontWeight] = useState('');
  const accessToken = localStorage.getItem('accessToken');
  const decodedToken = jwtDecode(accessToken);
  const token = `Bearer ${accessToken}`;

  const getRoom = async () => {
    try {
      const response = await axios.get(
        `https://team3-be-localzuhri.vercel.app/game/room/${roomId}`,
        {
          headers: { Authorization: `${token}` },
        },
      );
      setRoom(response.data);
      // console.log(response.data);
      return true;
    } catch (error) {
      // console.log(error);
      return error;
    }
  };

  const game = async (choice) => {
    // console.log(`in game${choice}`);
    try {
      const backendResponse = await axios.put(
        `https://team3-be-localzuhri.vercel.app/game/player1-vs-player2/${roomId}`,
        {
          pilihan_p2: choice,
        },
        {
          headers: { Authorization: `${token}` },
        },
      );
      // setpilihan_p1(backendResponse.data.Pilihan_Player_1);
      /* if(backendResponse.data.pemenang==="DRAW"||backendResponse.data.pemenang==="draw"){
        setResult("DRAW")
      }else{
        setResult(`${backendResponse.data.pemenang} WIN`)
      } */
      setTimeout(() => {
        if (backendResponse.data.message === 'Sukses Melawan') {
          navigate(`/complete-room/${roomId}`);
        }
      }, 2000);
      return true;
    } catch (error) {
      return error;
      // console.log(error.response.status); // Log the status code
      // console.log(error.response.data); // Log the error response data
      // console.log(error.response.headers);
    }
  };

  const handleImageClick = async (choice) => {
    if (pilihanP2 === '') {
      setPilihanP2(choice);
      setfontWeight('bold');
      setmiddleColor('green');
      setborder('none');
      await game(choice);
    }
  };

  useEffect(() => {
    getRoom();
    // game();
  }, []);

  useEffect(() => {
    // console.log(pilihan_p2);
  }, [pilihanP2, game, result, pilihanP1]);

  return (
    <div className="playerVsComContainer">
      <Nav />
      <h1 className="roomName" style={{ margin: '20px 0' }}>
        Room:
        {' '}
        {room && room.roomName}
      </h1>
      <div className="player1Vsplayer2-content-matchVsCom">
        <div className="player1Vsplayer2-content-leftVsCom">
          <div className="leftTitlePlayer">{room && room.username1}</div>
          <div className="player1Choices">
            <div>
              <img
                className="p1player"
                src={rock}
                alt="batu"
                style={{
                  backgroundColor: pilihanP2 && room.pilihanP1 === 'rock' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="p1player"
                src={paper}
                alt="kertas"
                style={{
                  backgroundColor: pilihanP2 && room.pilihanP1 === 'paper' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="p1player"
                src={scissors}
                alt="gunting"
                style={{
                  backgroundColor: pilihanP2 && room.pilihanP1 === 'scissors' ? '#ffb548' : '',
                }}

              />
            </div>
          </div>
        </div>
        <div className="player1Vsplayer2-content-middleVsCom">
          <div
            className="middleVsCom-box"
            style={{
              backgroundColor: middleColor,
              border,
              fontWeight,
            }}
          >
            {result === '' ? 'VS' : `${result}`}
          </div>
        </div>
        <div className="player1Vsplayer2-content-rightVsCom">
          <div className="rightTitlePlayer">{decodedToken.username}</div>
          <div className="comChoices">
            <div>
              <img
                className="p2player"
                src={rock}
                alt="batu"
                style={{ backgroundColor: pilihanP2 === 'rock' ? '#ffb548' : '' }}
                onClick={() => handleImageClick('rock')}

              />
            </div>
            <div>
              <img
                className="p2player"
                src={paper}
                alt="kertas"
                style={{
                  backgroundColor: pilihanP2 === 'paper' ? '#ffb548' : '',
                }}
                onClick={() => handleImageClick('paper')}
              />
            </div>
            <div>
              <img
                className="p2player"
                src={scissors}
                alt="gunting"
                style={{
                  backgroundColor: pilihanP2 === 'scissors' ? '#ffb548' : '',
                }}
                onClick={() => handleImageClick('scissors')}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AvailableRoom;
