/* eslint-disable global-require */
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import Nav from '../components/Nav';

const MyRoom = () => {
  const { roomId } = useParams();
  const [room, setRoom] = useState(null);

  const getRoom = async () => {
    const accessTokenYangTersimpan = localStorage.getItem('accessToken');
    const accessToken = `Bearer ${accessTokenYangTersimpan}`;
    try {
      const response = await axios.get(
        `https://team3-be-localzuhri.vercel.app/game/room/${roomId}`,
        {
          headers: { Authorization: `${accessToken}` },
        },
      );
      setRoom(response.data);
      return null; // Return a value (e.g., null) to satisfy the rule
    } catch (error) {
      return error;
    }
  };

  useEffect(() => {
    getRoom();
  }, []); // Run once on component mount

  return (
    <div className="playerVsComContainer">
      <Nav />
      <h1 className="roomName" style={{ margin: '20px 0' }}>
        Room:
        {' '}
        {room && room.roomName}
      </h1>
      <div className="player1Vsplayer2-content-matchVsCom">
        <div className="player1Vsplayer2-content-leftVsCom">
          <div className="leftTitlePlayer">{room && room.username1}</div>
          <div className="player1Choices">
            <div>
              <img
                className="complete-room-p1"
                src={require('../assets/batu.png')}
                alt="batu"
                style={{
                  backgroundColor:
                    room && room.pilihanP1 === 'rock' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="complete-room-p1"
                src={require('../assets/kertas.png')}
                alt="kertas"
                style={{
                  backgroundColor:
                    room && room.pilihanP1 === 'paper' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="complete-room-p1"
                src={require('../assets/gunting.png')}
                alt="gunting"
                style={{
                  backgroundColor:
                    room && room.pilihanP1 === 'scissors' ? '#ffb548' : '',
                }}
              />
            </div>
          </div>
        </div>
        <div className="player1Vsplayer2-content-middleVsCom">
          <div
            className="middleVsCom-box"
            style={{
              backgroundColor: '#2c2c2c',
              border: 'none',
              fontWeight: 'bold',
            }}
          >
            VS
          </div>
        </div>
        <div className="player1Vsplayer2-content-rightVsCom">
          <div className="rightTitlePlayer">WAITING for PLAYER 2</div>
          <div className="comChoices" style={{ opacity: '0.4' }}>
            <div>
              <img
                className="player1Vsplayer2-rock-rightVsCom"
                src={require('../assets/batu.png')}
                alt="batu"
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightVsCom"
                src={require('../assets/kertas.png')}
                alt="kertas"
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-rightVsCom"
                src={require('../assets/gunting.png')}
                alt="gunting"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="belowText-myRoom">
        <div className="belowText-myRoom-content">
          <span>
            <Link to="/create-room" style={{ textDecoration: 'none', color: '#ffb548' }}>
              Create another room
            </Link>
          </span>
          ..?!, or back to
          {' '}
          <span>
            <Link to="/dashboard" style={{ textDecoration: 'none', color: '#ffb548' }}>
              DASHBOARD
            </Link>
            ..?!
          </span>
        </div>
      </div>
    </div>
  );
};

export default MyRoom;
