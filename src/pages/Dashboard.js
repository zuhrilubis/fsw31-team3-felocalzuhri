import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../components/Button';
import Biodata from '../components/Biodata';
import Nav from '../components/Nav';
import RoomList from '../components/RoomList';
import HistoryIcon from '../components/icons/HistoryIcon';
import ControllerIcon from '../components/icons/ControllerIcon';
import PlusIcon from '../components/icons/PlusIcon';

const Dashboard = () => (
  <div className="dashboard">
    <Nav />
    <div className="dashboard-content">
      <div className="game">
        <div className="upper-game">
          <Link to="/vs-com" style={{ textDecoration: 'none' }}>
            <button className="play-vs-com" type="button">
              <ControllerIcon />
              {' '}
              PLAY VS COM
            </button>
          </Link>
          <div className="create-room">
            <Link to="/create-room" style={{ textDecoration: 'none' }}>
              <Button title="Create Room" icon={<PlusIcon />} />
            </Link>
          </div>
        </div>
        <hr
          style={{
            border: '1px solid',
            color: 'grey',
            backgroundColor: 'white',
            width: '100%',
          }}
        />
        <RoomList />
      </div>
      <div className="biodata">
        <div>
          <Biodata />
        </div>
        <Link to="/game-history" style={{ textDecoration: 'none' }}>
          <Button title="game history" icon={<HistoryIcon />} />
        </Link>
      </div>
    </div>
  </div>
);

export default Dashboard;
