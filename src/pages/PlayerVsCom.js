/* eslint-disable global-require */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useSelector } from 'react-redux';
import Nav from '../components/Nav';

const PlayerVsComPage = () => {
  const [playerChoice, setplayerChoice] = useState('');
  const [comChoice, setcomChoice] = useState('');
  const [middleColor, setmiddleColor] = useState('');
  const [border, setborder] = useState('');
  const [result, setresult] = useState('');
  const [fontWeight, setfontWeight] = useState('');
  const [dataVsCom, setDataVsCom] = useState('');
  // It shows username of the current user
  const userRedux = useSelector((state) => state.user);

  const pilihanRandomCom = () => {
    const pilihan = ['rock', 'paper', 'scissors'];
    setcomChoice(pilihan[Math.floor(Math.random() * pilihan.length * 1)]);
  };

  const postVsCom = async () => {
    const accessToken = localStorage.getItem('accessToken');
    try {
      const responseAPI = await axios.post(
        'https://team3-be-localzuhri.vercel.app/game/player-vs-com',
        {
          pilihanP1: playerChoice,
          pilihanP2: comChoice,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        },
      );
      setDataVsCom(responseAPI.data);
      // console.log(dataVsCom);
      return true;
    } catch (err) {
      // console.error(err);
      return err;
    }
  };

  useEffect(() => {
    if (
      (playerChoice === 'rock' && comChoice === 'rock')
      || (playerChoice === 'scissors' && comChoice === 'scissors')
      || (playerChoice === 'paper' && comChoice === 'paper')
    ) {
      setresult('DRAW');
      postVsCom();
    } else if (
      (playerChoice === 'rock' && comChoice === 'scissors')
      || (playerChoice === 'scissors' && comChoice === 'paper')
      || (playerChoice === 'paper' && comChoice === 'rock')
    ) {
      setresult('YOU WIN');
      postVsCom();
    } else if (
      (playerChoice === 'rock' && comChoice === 'paper')
      || (playerChoice === 'scissors' && comChoice === 'rock')
      || (playerChoice === 'paper' && comChoice === 'scissors')
    ) {
      setresult('COM WIN');
      postVsCom();
    }
  }, [playerChoice, comChoice]);

  const handleClick = (e) => {
    if (playerChoice === '') {
      setplayerChoice(e.target.alt);
      pilihanRandomCom();
      setfontWeight('bold');
      setmiddleColor('green');
      setborder('none');
    }
  };

  // console.log(`pilihan_p1: ${playerChoice}`);
  // console.log(`pilihan_p2: ${comChoice}`);

  const handleRefresh = () => {
    setplayerChoice('');
    setcomChoice('');
    setmiddleColor('');
    setborder('');
    setresult('');
  };

  return (
    <div className="playerVsComContainer">
      <Nav />
      <div className="player1Vsplayer2-content-matchVsCom">
        <div className="player1Vsplayer2-content-leftVsCom">
          <div className="leftTitlePlayer">{userRedux.username}</div>
          <div className="player1Choices">
            <div>
              <img
                className="player1Vsplayer2-rock-leftVsCom"
                src={require('../assets/batu.png')}
                alt="rock"
                style={{
                  backgroundColor: playerChoice === 'rock' ? '#ffb548' : '',
                }}
                onClick={handleClick}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-leftVsCom"
                src={require('../assets/kertas.png')}
                alt="paper"
                style={{
                  backgroundColor: playerChoice === 'paper' ? '#ffb548' : '',
                }}
                onClick={handleClick}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-leftVsCom"
                src={require('../assets/gunting.png')}
                alt="scissors"
                style={{
                  backgroundColor: playerChoice === 'scissors' ? '#ffb548' : '',
                }}
                onClick={handleClick}
              />
            </div>
          </div>
        </div>
        <div className="player1Vsplayer2-content-middleVsCom">
          <div
            className="middleVsCom-box"
            style={{
              backgroundColor: middleColor,
              border,
              fontWeight,
            }}
          >
            {result === '' ? 'VS' : `${result}`}
          </div>
        </div>
        <div className="player1Vsplayer2-content-rightVsCom">
          <div className="rightTitlePlayer">COM</div>
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightVsCom"
                src={require('../assets/batu.png')}
                alt="batu"
                style={{
                  backgroundColor: comChoice === 'rock' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightVsCom"
                src={require('../assets/kertas.png')}
                alt="kertas"
                style={{
                  backgroundColor: comChoice === 'paper' ? '#ffb548' : '',
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-rightVsCom"
                src={require('../assets/gunting.png')}
                alt="gunting"
                style={{
                  backgroundColor: comChoice === 'scissors' ? '#ffb548' : '',
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="vsComRefresh">
        <img
          className="vsComTombolRefresh"
          src={require('../assets/refresh.png')}
          alt=""
          onClick={handleRefresh}
        />
      </div>
      <div className="vsComBottomText">
        {result === 'COM WIN' && (
          <div className="vsComBottomTextResult">
            DON&apos;T GIVE UP, TRY AGAIN..GOOD LUCK..!
          </div>
        )}
        {result === 'YOU WIN' && (
          <div className="vsComBottomTextResult">
            CONGRATULATION..!! YOU WIN..
          </div>
        )}
        {result === 'DRAW' && (
          <div className="vsComBottomTextResult">
            ALMOST.., TRY AGAIN..GOOD LUCK..!
          </div>
        )}
        {result === '' && (
          <div className="vsComBottomTextReady">
            ARE YOU READY TO FIGHT..?!!
            <br />
            Choose your choice on the left
            side, Please..!
          </div>
        )}
      </div>
    </div>
  );
};

export default PlayerVsComPage;
