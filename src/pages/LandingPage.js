/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import { Link } from 'react-router-dom';
import Nav from '../components/Nav';
import Slideshow from '../components/Slideshow';
import ProfilePictCard1 from '../components/card-contents/ProfilePictCard1';
import TwitterIconCard from '../components/card-contents/TwitterIconCard';
import ProfilePictCard2 from '../components/card-contents/ProfilePictCard2';
import ProfilePictCard3 from '../components/card-contents/ProfilePictCard3';
import Skull from '../components/Skull';
import FacebookIcon from '../components/social-media-contents/FacebookIcon';
import TwitterIcon from '../components/social-media-contents/TwitterIcon';
import YoutubeIcon from '../components/social-media-contents/YoutubeIcon';
import TwitchIcon from '../components/social-media-contents/TwitchIcon';

const LandingPage = () => {
  const accessToken = localStorage.getItem('accessToken');
  let mainButton;

  if (accessToken) {
    mainButton = 'play now';
  } else if (!accessToken) {
    mainButton = 'sign in';
  }

  return (
    <div>
      <div className="bg-image-1">
        {/* Navbar  */}
        <Nav />

        {/* Main Content */}
        <div className="content d-flex flex-column justify-content-center align-items-center position-relative">
          <h1 className="font-bebas-neue fw-bold h1-lp">
            PLAY TRADITIONAL GAME
          </h1>
          <h3 className="experience">Experience new traditional game play</h3>
          <div
            className="d-grid gap-2 col-md-2 mx-auto"
            style={{ marginBottom: '160px', marginTop: '40px' }}
          >
            <button
              className="button-lp btn btn-warning text-uppercase fw-bold py-3"
              type="button"
            >
              <Link
                to="/dashboard"
                style={{ textDecoration: 'none', color: 'black' }}
              >
                {mainButton}
              </Link>
            </button>
          </div>
          <div
            className="d-flex flex-column align-items-center fw-semibold position-absolute bottom-0"
            style={{ marginBottom: '50px' }}
          >
            <p
              className="align-items-start text-uppercase mb-0 fw-bold"
              style={{ fontSize: '12px' }}
            >
              the story
            </p>
          </div>
        </div>
      </div>

      {/* Second Content */}
      <div className="bg-image-2">
        <div
          className="second-content d-flex flex-row justify-content-center align-items-center "
          style={{ height: '100%' }}
        >
          <div className="the-games d-flex flex-column justify-content-start">
            <h3 className="h3-lp">What&apos;s so special?</h3>
            <h2 className="h2-lp text-uppercase">the games</h2>
          </div>
          <div className="right-side" style={{ marginBottom: '70px' }}>
            {/* Slideshow container */}
            <div className="slideshow-container">
              <Slideshow />
            </div>
          </div>
        </div>
      </div>

      {/* Third Content */}
      <div className="bg-image-3">
        <div className="third-content-container d-flex align-items-center justify-content-center">
          <div className="d-flex flex-column mb-5">
            <div className="features-container mb-5 ms-3">
              <h3 className="h3-lp">What&apos;s so special?</h3>
              <h2 className="text-uppercase">features</h2>
            </div>
            <div className="mt-4">
              <ul className="features-list">
                <li className="vertical-line traditional-games orange-text mb-3">
                  <h3 className="h3-lp">traditional games</h3>
                </li>
                <p className="mb-5">
                  If you miss your childhood, we provide
                  <br />
                  many traditional games here
                </p>
                <li className="vertical-line orange-text pt-2 mb-5">
                  <h3 className="h3-lp">game suit</h3>
                </li>
                <li className="vertical-line orange-text">
                  <h3 className="h3-lp">tbd</h3>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      {/* Fourth Content */}
      <div className="bg-image-4">
        <div className="blank">
          <div className="part1-4thcontent d-flex justify-content-center align-items-end text-capitalize">
            <p>can my computer run this game?</p>
          </div>
          <div className="part2-4thcontent d-flex flex-column position-absolute end-0">
            <h2 className="text-uppercase">
              system
              <br />
              requirements
            </h2>

            {/* Table */}
            <table className="table-requirements mt-5 table-lp">
              <tr>
                <td className="table-lp">
                  <h3 className="orange-text h3-lp">os :</h3>
                  <p>
                    Windows 7 64-bit only
                    <br />
                    (No OSX support at this time)
                  </p>
                </td>
                <td className="table-lp">
                  <h3 className="orange-text h3-lp">processor :</h3>
                  <p>
                    Intel Core 2 Duo @ 2.4 GHZ or
                    <br />
                    AMD Athlon X2 @ 2.8 GHZ
                  </p>
                </td>
              </tr>
              <tr>
                <td className="table-lp">
                  <h3 className="orange-text h3-lp">memory :</h3>
                  <p>4 GB RAM</p>
                </td>
                <td className="table-lp">
                  <h3 className="orange-text h3-lp">storage :</h3>
                  <p>8 GB available space</p>
                </td>
              </tr>
              <tr>
                <td colSpan="2" className="table-lp">
                  <h3 className="orange-text h3-lp">graphics :</h3>
                  <p>
                    NVIDIA GeForce GTX 660 2GB or
                    <br />
                    AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)
                  </p>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      {/* Fifth Content */}
      <div className="bg-image-5">
        <div className="fifth-content d-flex">
          {/* Top Score */}
          <div className="top-scores-1 d-flex flex-column justify-content-center">
            <h2 className="text-uppercase mb-3">top scores</h2>
            <p className="my-3">
              This top score from various games provided
              <br />
              on this platform
            </p>
            <div className="d-grid gap-2 col-5 mt-3">
              <button
                className="button-lp btn btn-warning fw-bold py-3"
                type="button"
              >
                see more
              </button>
            </div>
          </div>

          {/* Card */}
          <div className="top-scores-2 d-flex flex-column justify-content-center">
            {/* Card 1 */}
            <div
              className="card card-1 mb-3 align-self-end"
              style={{ height: '21vh', marginRight: '120px' }}
            >
              <div className="card-body d-flex flex-column justify-content-center">
                <div className="d-flex">
                  <ProfilePictCard1 />
                  <div className="ms-4">
                    <h5 className="card-title orange-text text-capitalize">
                      Evan Lahti
                    </h5>
                    <h6 className="card-subtitle mb-2 text-muted">PC Gamer</h6>
                  </div>
                  <TwitterIconCard />
                </div>
                <p className="card-text mt-2 mb-3">
                  &quot;One of my gaming highlights of the year.&quot;
                </p>
                <p className="date-topscore text-muted">18 June 2021</p>
              </div>
            </div>

            {/* Card 2 */}
            <div className="card card-2 my-3">
              <div className="card-body d-flex flex-column justify-content-center">
                <div className="d-flex">
                  <ProfilePictCard2 />
                  <div className="ms-4">
                    <h5 className="card-title orange-text text-capitalize">
                      Jada Griffin
                    </h5>
                    <h6 className="card-subtitle mb-2 text-muted">
                      Nerdreactor
                    </h6>
                  </div>
                  <TwitterIconCard />
                </div>
                <p className="card-text mt-2 mb-3">
                  &quot;The next big thing in the world of streaming
                  <br />
                  and survival games.&quot;
                </p>
                <p className="date-topscore text-muted">10 July 2021</p>
              </div>
            </div>

            {/* Card 3 */}
            <div
              className="card card-3 mt-3 align-self-end"
              style={{ marginRight: '120px' }}
            >
              <div className="card-body d-flex flex-column justify-content-center">
                <div className="d-flex">
                  <ProfilePictCard3 />
                  <div className="ms-4">
                    <h5 className="card-title orange-text text-capitalize">
                      Aaron Williams
                    </h5>
                    <h6 className="card-subtitle mb-2 text-muted">Uproxx</h6>
                  </div>
                  <TwitterIconCard />
                </div>
                <p className="card-text mt-2 mb-3">
                  &quot;Snoop Dogg playing the wildly entertaining
                  <br />
                  &apos;SOS&apos; is ridiculous.&quot;
                </p>
                <p className="date-topscore text-muted">24 December 2018</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Sixth Content */}
      <div className="bg-image-6">
        {/* Newsletter and Skull */}
        <div
          className="part1-6thcontent d-flex justify-content-center"
          style={{ height: '110vh', marginLeft: '100px', marginBottom: '70px' }}
        >
          <Skull />
          <div
            className="newsletter d-flex flex-column justify-content-center"
            style={{ marginLeft: '120px', marginTop: '50px' }}
          >
            <h3 className="mb-2 h3-lp">
              Want to stay in
              <br />
              touch?
            </h3>
            <h2 className="text-uppercase my-2">
              newsletter
              <br />
              subscribe
            </h2>
            <p className="mt-2 mb-4">
              In order to start receiving our news, all you have to do is enter
              your email
              <br />
              address. Everything else will be taken care of by us. We will send
              you emails
              <br />
              containing information about game. We don&apos;t spam.
            </p>
            <div className="form-container-lp d-flex mt-5">
              <form className="form-floating d-grid col-7">
                <input
                  type="email"
                  className="form-control orange-text text-lowercase"
                  id="floatingInputValue"
                  placeholder="name@example.com"
                  value="youremail@binar.co.id"
                />
                <label htmlFor="floatingInputValue" className="text-muted">
                  Your email address
                </label>
              </form>
              <div className="d-grid gap-2 col-5 ms-3">
                <button
                  className="button-lp btn btn-warning fw-bold py-3 text-capitalize"
                  type="button"
                >
                  subscribe now
                </button>
              </div>
            </div>
          </div>
        </div>

        {/* Footer */}
        <div
          className="footer d-flex flex-column"
          style={{ height: '20vh', margin: '0 84px', fontSize: '16px' }}
        >
          <div
            className="part1-footer d-flex align-items-center justify-content-end"
            style={{ height: '10vh' }}
          >
            <div className="footer-nav d-flex me-5 mt-3 flex-row align-self-center">
              <p className="text-uppercase">
                <Link to="/" className="footer-list mx-4" aria-current="page">
                  main
                </Link>
              </p>
              <p className="text-uppercase">
                <Link to="/" className="footer-list mx-4">
                  about
                </Link>
              </p>
              <p className="text-uppercase">
                <Link to="/" className="footer-list mx-4">
                  game features
                </Link>
              </p>
              <p className="text-uppercase">
                <Link to="/" className="footer-list mx-4">
                  system requirements
                </Link>
              </p>
              <p className="text-uppercase">
                <Link to="/" className="footer-list ms-4">
                  quotes
                </Link>
              </p>
            </div>
            <div className="socialmedia d-flex ms-5 align-self-center">
              <FacebookIcon />
              <TwitterIcon />
              <YoutubeIcon />
              <TwitchIcon />
            </div>
          </div>
          <hr />
          <div
            className="part2-footer d-flex"
            style={{ height: '10vh', fontSize: '14px' }}
          >
            <p className="text-muted me-auto">
              © 2018 Your Games, Inc. All Rights Reserved
            </p>
            <div className="privacy-container text-uppercase ms-auto">
              <Link to="/" className="footer-list">
                privacy policy |
              </Link>
              <Link
                to="/"
                className="footer-list"
                style={{ textDecoration: 'none' }}
              >
                terms of services |
              </Link>
              <Link to="/" className="footer-list">
                code of conduct
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
