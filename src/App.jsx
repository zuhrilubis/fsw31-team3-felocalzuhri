import React from 'react';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import Login from './pages/Login';
import Registration from './pages/Registration';
import Dashboard from './pages/Dashboard';
import CreateRoom from './pages/CreateRoom';
import LandingPage from './pages/LandingPage';
import PlayerVsComPage from './pages/PlayerVsCom';
import GameHistyoryPage from './pages/GameHistory';
import AvailableRoom from './pages/AvailableRoom';
import MyRoom from './pages/MyRoom';
import CompleteRoom from './pages/CompleteRoom';
import Authentication from './components/Authentication';

const App = () => (
  <Routes>
    <Route path="/" element={<LandingPage />} />
    <Route
      path="/login"
      element={<Login />}
    />
    <Route path="/register" element={<Registration />} />
    <Route
      path="/dashboard"
      element={(
        <Authentication>
          <Dashboard />
        </Authentication>
        )}
    />
    <Route
      path="/vs-com"
      element={
        <PlayerVsComPage />
        }
    />
    <Route
      path="/create-room"
      element={
        <CreateRoom />
        }
    />
    <Route
      path="/game-history"
      element={
        <GameHistyoryPage />
        }
    />
    <Route
      path="/available-room/:roomId"
      element={
        <AvailableRoom />
        }
    />
    <Route
      path="/my-room/:roomId"
      element={
        <MyRoom />
        }
    />
    <Route
      path="/complete-room/:roomId"
      element={
        <CompleteRoom />
        }
    />
    <Route path="*" element={<center>404 page is not found</center>} />
  </Routes>
);

export default App;
